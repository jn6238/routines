// Styled-components import
import styled from 'styled-components';

export const LayoutContainer = styled.div`
  background: rgb(62, 9, 75);
  background: linear-gradient(145deg, rgba(62, 9, 75, 1) 0%, rgba(45, 25, 148, 1) 47%, rgba(0, 99, 255, 1) 100%);
`;

export const Overlay = styled.div`
  width: 100%;
  height: 100%;
  position: absolute;
  background-color: #000;
  opacity: 0.5;
  z-index: 1040;
`;

export const ToolbarIcon = styled.i`
  color: rgb(0, 123, 255);
`;

export const Navbar = styled.nav`
  height: 50px;
`;

export const ProjectContainer = styled.div`
  padding-top: 50px;
  min-height: calc(100vh - 80px);
`;
