// React import
import React from 'react';
import { withRouter } from 'react-router-dom';
// Styled-component imports
import { LayoutContainer, Overlay, ToolbarIcon, Navbar, ProjectContainer } from './styles';
// Component imports
import HelpModal from './HelpModal/HelpModal';
import Footer from '../Footer/Footer';

/**
 * Layout component of the application
 */
class Layout extends React.Component {
  state = {
    // Flag to indicate whether the help modal should be displayed or not
    helpModalOpen: false
  };

  /**
   * Open or close the help modal
   */
  toggleHelpModalHandler = () => {
    this.setState((prevState) => {
      return {
        ...prevState,
        helpModalOpen: !prevState.helpModalOpen
      };
    });
  };

  /**
   * Create the navigation bar for the application
   */
  createNavbar = () => {
    return (
      <Navbar className='navbar navbar-expand-sm navbar-dark bg-dark p-0 fixed-top'>
        <div className='container my-auto'>
          <button className='btn' onClick={() => this.props.history.push('/projects')}>
            <ToolbarIcon className='fas fa-home fa-2x' />
          </button>
          <button className='btn' onClick={this.toggleHelpModalHandler}>
            <ToolbarIcon className='fas fa-question-circle fa-2x' />
          </button>
        </div>
      </Navbar>
    );
  };

  /**
   * Render the Layout component
   */
  render() {
    return (
      <LayoutContainer>
        {this.state.helpModalOpen ? <Overlay /> : null}
        {this.createNavbar()}
        <ProjectContainer>{this.props.children}</ProjectContainer>
        <Footer />
        {this.state.helpModalOpen ? <HelpModal toggleModalHandler={this.toggleHelpModalHandler} /> : null}
      </LayoutContainer>
    );
  }
}

export default withRouter(Layout);
