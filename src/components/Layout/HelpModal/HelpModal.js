// React import
import React from 'react';

// Styled-component imports
import { HelpModalContainer } from './styles';

// The text to display on the help modal
const helpModalText = (
  <div className='modal-body'>
    <p>
      Create your own spaceship routine by adding or removing command blocks in the routine, while specifying what
      command should be performed on each block.
    </p>
    <p>
      After you've created your spaceship routine, watch your spaceship fly by clicking on the "Play Routine" button.
    </p>
    <p> Be careful though, you don't want your spaceship to crash!</p>
  </div>
);

/**
 * Help modal which explains how to use the application
 */
const HelpModal = (props) => {
  return (
    <HelpModalContainer>
      <div className='modal-dialog'>
        <div className='modal-content'>
          <div className='modal-header'>
            <h5 className='modal-title'>Welcome to "Fly Your Spaceship"</h5>
            <button className='close' onClick={props.toggleModalHandler}>
              &times;
            </button>
          </div>
          {helpModalText}
          <div className='modal-footer'>
            <button className='btn btn-primary' onClick={props.toggleModalHandler}>
              Close
            </button>
          </div>
        </div>
      </div>
    </HelpModalContainer>
  );
};

export default HelpModal;
