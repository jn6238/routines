// React imports
import React from 'react';
// Styled-component imports
import { SnackbarContainer } from './styles';

/**
 * Snackbar component
 */
const Snackbar = (props) => {
  return (
    <SnackbarContainer className='bg-success' show={props.show}>
      {props.message}
    </SnackbarContainer>
  );
};

export default Snackbar;
