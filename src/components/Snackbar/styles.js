// Styled-components import
import styled from 'styled-components';

export const SnackbarContainer = styled.div`
  visibility: ${props => (props.show ? 'visible' : 'hidden')};
  min-width: 250px;
  margin-left: -125px;
  color: #fff;
  text-align: center;
  border-radius: 2px;
  padding: 16px;
  position: fixed;
  z-index: 1;
  left: 10%;
  bottom: 100px;
  font-size: 1rem;
`;
