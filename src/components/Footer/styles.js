// Styled-components import
import styled from 'styled-components';

export const FooterContainer = styled.footer`
  height: 70px;
  margin-top: 10px;
`;
