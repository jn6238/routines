// React import
import React from 'react';
// Styled-component imports
import { FooterContainer } from './styles';

class Footer extends React.Component {
  render() {
    // Get the current year to add to the copyright text
    const year = new Date().getFullYear();

    return (
      <FooterContainer className='bg-dark text-white text-center p-4'>
        <div className='container'>
          <div className='row'>
            <div className='col'>
              <p>
                Copyright &copy; Johann Mey <span>{year}</span>
              </p>
            </div>
          </div>
        </div>
      </FooterContainer>
    );
  }
}

export default Footer;
