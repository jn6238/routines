// Styled-components import
import styled from 'styled-components';

export const ProjectsContainer = styled.div`
  padding-top: 80px;
  min-height: calc(100vh - 80px);
`;

export const ProjectCard = styled.button`
  display: flex;
  flex-direction: column;
  align-items: center;
  background-color: ${(props) => props.color} !important;
`;
