// React imports
import React from 'react';
// Component imports
import LayoutContainer from '../../components/Layout/Layout';
// Styled-component imports
import {} from './styles';

/**
 *  Component which displays weather information
 */
class Weather extends React.Component {
  render() {
    return <LayoutContainer></LayoutContainer>;
  }
}

export default Weather;
