// React import
import React from 'react';
// Project imports
import Routines from './Routines/Routines';
import Weather from './Weather/Weather';
// Component imports
import Footer from '../components/Footer/Footer';
// Styled-component imports
import { ProjectsContainer, ProjectCard } from './styles';
// Resource imports
import Rocketship from '../assets/red-rocket-ship.jpg';
import HeavyClouds from '../assets/heavy-clouds.jpg';

// Array of projects
export const projects = [
  {
    name: 'Fly Your Spaceship',
    component: Routines,
    path: '/fly-your-spaceship',
    image: Rocketship,
    alt: 'Spaceship',
    color: '#0339fc',
  },
  {
    name: 'Weather Forecast',
    component: Weather,
    path: '/weather-forecast',
    image: HeavyClouds,
    alt: 'Sun',
    color: '#b0071b',
  },
];

/**
 * The home component which displays all the projects which can be viewed
 */
class Projects extends React.Component {
  projectCard = (project) => {
    return (
      <ProjectCard
        key={project.path}
        className='col-md-4 col-sm-6 m-3 card text-white'
        color={project.color}
        onClick={() => this.props.history.push(project.path)}
      >
        <h3>{project.name}</h3>
        <img className='img-fluid py-2' src={project.image} alt={project.alt} />
      </ProjectCard>
    );
  };

  render() {
    return (
      <div>
        <header className='py-2 bg-dark text-white fixed-top'>
          <div className='container'>
            <div className='row'>
              <h1>Mey Projects</h1>
            </div>
          </div>
        </header>
        <ProjectsContainer className='container'>
          <div className='row'>{projects.map(this.projectCard)}</div>
        </ProjectsContainer>
        <Footer />
      </div>
    );
  }
}

export default Projects;
