// React imports
import React from 'react';
// Styled-component imports
import { SpaceshipAreaContainer, SpaceshipIcon, CrashMessageContainer, CrashMessage, StyledButton } from './styles';
// Constant imports
import { BlockCommands } from '../RoutineBlock/RoutineBlock';
// Utility imports
import { isArrayEmpty } from '../../../utils/GeneralUtils';

// Boundary constants
const MIN_HORIZONTAL_POSITION = -15;
const MAX_HORIZONTAL_POSITION = 945;
const MIN_VERTICAL_POSITION = 5;
const MAX_VERTICAL_POSITION = 465;

/**
 * Component that is displayed when in play view, which plays the current record's routine
 */
class RoutinesPlay extends React.Component {
  state = {
    // The angle of the spaceship
    angle: 0,
    // Flag to indicate whether to show the crash message
    crash: false,
    // Flag to indicate whether to play or stop the routine
    play: true,
    // The horizontal position of the spaceship
    positionX: 100,
    // The vertical position of the spaceship
    positionY: 230,
    // A counter which is used to determine which command item to get from the current record's routine array
    timeCount: 0,
  };

  initialState = { ...this.state };

  // ID of the interval which is used to calculate the movement of the spaceship
  intervalId = 0;

  componentDidMount() {
    this.intervalId = window.setInterval(() => {
      if (this.state.play && !isArrayEmpty(this.props.currentRecord.routine)) {
        // Calculate which command in the routine array of the current record to execute.
        // If the command is "FORWARD" the spaceship should move forward 100 pixels before executing the next command.
        const commandIndex = Math.floor(this.state.timeCount / 100);

        if (commandIndex >= this.props.currentRecord.routine.length) {
          // If the last command in the routine array of the current record has been executed, stop the spaceship.
          this.setState((prevState) => {
            return {
              ...prevState,
              play: false,
            };
          });
        } else {
          // Get the command to execute from the routine array of the current record.
          const currentCommand = this.props.currentRecord.routine[commandIndex];

          let { positionX, positionY, angle, timeCount } = this.state;

          switch (currentCommand) {
            case BlockCommands.right:
              // Turn the spaceship right, and move to the next command.
              angle = (angle + 90) % 360;
              timeCount += 100;
              break;
            case BlockCommands.left:
              // Turn the spaceship left, and move to the next command.
              angle = (angle - 90) % 360;
              timeCount += 100;
              break;
            case BlockCommands.forward:
            default:
              // Move the spaceship forward by using the current angle of the spaceship to calculate the next coordinates.
              positionX += Math.cos((this.state.angle * Math.PI) / 180);
              positionY += Math.sin((this.state.angle * Math.PI) / 180);
              timeCount += 1;
              break;
          }

          if (
            positionX < MIN_HORIZONTAL_POSITION ||
            positionX > MAX_HORIZONTAL_POSITION ||
            positionY < MIN_VERTICAL_POSITION ||
            positionY > MAX_VERTICAL_POSITION
          ) {
            // If the new position of the spaceship falls outside the boundaries, stop the routine and display the crash message.
            this.setState((prevState) => {
              return {
                ...prevState,
                crash: true,
                play: false,
              };
            });
          } else {
            // If the new position of the spaceship falls within the boundaries, update the position and angle of the spaceship.
            this.setState((prevState) => {
              return {
                ...prevState,
                angle,
                positionX,
                positionY,
                timeCount,
              };
            });
          }
        }
      }
    }, 1000 / 60);
  }

  /**
   * On component unmount, clear the interval
   */
  componentWillUnmount() {
    clearInterval(this.intervalId);
  }

  /**
   * Stops or starts the routine, by resetting the state and inverting the play state flag
   */
  togglePlayClickedHandler = () => {
    this.setState((prevState) => {
      return {
        ...this.initialState,
        play: !prevState.play,
      };
    });
  };

  /**
   * Create a header to indicate which routine is being playing
   */
  createHeader = () => {
    return (
      <header id='main-header' className='py-2 bg-success text-white'>
        <div className='container'>
          <div className='row'>
            <div className='col-md-6'>
              <h1>{`Play - ${this.props.currentRecord.name}`}</h1>
            </div>
          </div>
        </div>
      </header>
    );
  };

  /**
   * Create the play view action buttons
   */
  createActions = () => {
    // Create the back button which, when clicked on, navigates the user back to the edit view
    const backButton = (
      <button id='back-icon' className='btn btn-light btn-block' onClick={this.props.backClickedHandler}>
        <i className='fas fa-undo' /> To Edit Mode
      </button>
    );

    // Create the play button which, when clicked on, plays the current record's routine.
    // The button is disabled when the routine is playing.
    const playButton = (
      <StyledButton
        id='play-icon'
        className='btn btn-success btn-block'
        onClick={this.togglePlayClickedHandler}
        disabled={this.state.play}
        disableCursor={this.state.play}
      >
        <i className='fas fa-play' /> Play Routine
      </StyledButton>
    );

    // Create the play button which, when clicked on, stops the current record's routine.
    // The button is disabled when the routine is not playing.
    const stopButton = (
      <StyledButton
        id='stop-icon'
        className='btn btn-danger btn-block'
        onClick={this.togglePlayClickedHandler}
        disabled={!this.state.play}
        disableCursor={!this.state.play}
      >
        <i className='fas fa-stop' /> Stop Routine
      </StyledButton>
    );

    return (
      <section id='actions' className='py-4 mb-4'>
        <div className='container'>
          <div className='row'>
            <div className='col-md-3'>{backButton}</div>
            <div className='col-md-3'>{playButton}</div>
            <div className='col-md-3'>{stopButton}</div>
          </div>
        </div>
      </section>
    );
  };

  /**
   * Create the animation area with the spaceship icon that moves based upon the calculated state coordinates and angle
   */
  createAnimation = () => {
    return (
      <SpaceshipAreaContainer className='container bg-dark'>
        <SpaceshipIcon
          className='fas fa-space-shuttle fa-2x'
          color={this.props.currentRecord.color}
          style={{
            transform: `translate3d(${this.state.positionX}px, ${this.state.positionY}px, 0) rotate(${this.state.angle}deg)`,
          }}
        />
      </SpaceshipAreaContainer>
    );
  };

  /**
   * Create the message that is displayed when the spaceship has "crashed"
   */
  createCrashMessage = () => {
    return (
      <CrashMessageContainer className='container bg-dark'>
        <CrashMessage>CRASH</CrashMessage>
      </CrashMessageContainer>
    );
  };

  /**
   * Render the RoutinesPlay component
   */
  render() {
    return (
      <div>
        {this.createHeader()}
        {this.createActions()}
        {this.state.crash ? this.createCrashMessage() : this.createAnimation()}
      </div>
    );
  }
}

export default RoutinesPlay;
