// Styled-components import
import styled from 'styled-components';

export const SpaceshipAreaContainer = styled.div`
  height: 500px !important;
  max-width: 1000px !important;
`;

export const CrashMessageContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 500px !important;
  max-width: 1000px !important;
`;

export const SpaceshipIcon = styled.i`
  color: ${(props) => props.color};
  position: relative;
`;

export const CrashMessage = styled.div`
  font-size: 10rem;
  color: red;
  width: 500px;
`;

export const StyledButton = styled.button`
  cursor: ${(props) => (props.disableCursor ? 'auto' : 'pointer')} !important;
`;
