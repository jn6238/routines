// React import
import React from 'react';

// Styled-component imports
import { ErrorModalContainer } from './styles';

/**
 * Error modal to indicate that an error occurred while attempting to make an Ajax call to the database
 */
const ErrorModal = (props) => {
  return (
    <ErrorModalContainer>
      <div className='modal-dialog'>
        <div className='modal-content'>
          <div className='modal-header'>
            <h5 className='modal-title'>Error</h5>
            <button className='close' onClick={this.props.closeModalHandler}>
              &times;
            </button>
          </div>
          <div className='modal-body'>
            An error occurred while trying to connect to the database. Please try again later.
          </div>
          <div className='modal-footer'>
            <button className='btn btn-danger' onClick={this.props.closeModalHandler}>
              Close
            </button>
          </div>
        </div>
      </div>
    </ErrorModalContainer>
  );
};

export default ErrorModal;
