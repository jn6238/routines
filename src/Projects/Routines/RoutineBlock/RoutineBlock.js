// React imports
import React from 'react';

export const BlockCommands = {
  start: 'START',
  forward: 'FORWARD',
  left: 'LEFT',
  right: 'RIGHT'
};

/**
 * Component that is used to build a spaceship routine
 */
class RoutineBlock extends React.Component {
  /**
   * Get the routine block color to display based upon the block's command
   */
  getBlockColor = () => {
    switch (this.props.blockCommand) {
      case BlockCommands.start:
        return 'bg-success';
      case BlockCommands.right:
        return 'bg-warning';
      case BlockCommands.left:
        return 'bg-info';
      case BlockCommands.forward:
      default:
        return 'bg-primary';
    }
  };

  /**
   * Create the read-only text for the start block
   */
  createStartBlockText = () => {
    return (
      <div className='form-group my-auto'>
        <input id='startBlockCommand' type='text' className='form-control' placeholder={BlockCommands.start} readOnly />
      </div>
    );
  };

  /**
   * Create the block command selection field
   */
  createBlockSelection = () => {
    return (
      <div className='form-group my-auto'>
        <select
          disabled={this.props.blockCommand === BlockCommands.start}
          className='form-control'
          id='blockCommand'
          onChange={(event) => this.props.blockCommandChangedHandler(event.target.value)}
          value={this.props.blockCommand}
        >
          <option value={BlockCommands.forward}>{BlockCommands.forward}</option>
          <option value={BlockCommands.left}>{BlockCommands.left}</option>
          <option value={BlockCommands.right}>{BlockCommands.right}</option>
        </select>
      </div>
    );
  };

  /**
   * Create the delete button on the routine blocks
   */
  createDeleteBlockButton = () => {
    return (
      <div className='row p-2'>
        <button id='minus-icon' className='btn btn-dark m-auto' onClick={this.props.deleteClickedHandler}>
          <i className='fas fa-minus' />
        </button>
      </div>
    );
  };

  /**
   * Create the add button on the routine blocks
   */
  createAddBlockButton = () => {
    return (
      <div className='row p-2'>
        <button id='plus-icon' className='btn btn-dark m-auto' onClick={this.props.addClickedHandler}>
          <i className='fas fa-plus' />
        </button>
      </div>
    );
  };

  /**
   * Render the RoutineBlock component
   */
  render() {
    const blockCommandField =
      this.props.blockCommand === BlockCommands.start ? this.createStartBlockText() : this.createBlockSelection();

    return (
      <div className={`${this.getBlockColor()} my-2 p-2 rounded`}>
        <div className='row'>
          <div className='my-auto col-md-8'>{blockCommandField}</div>
          <div className='col-md-4'>
            {this.props.blockCommand === BlockCommands.start ? null : this.createDeleteBlockButton()}
            {this.createAddBlockButton()}
          </div>
        </div>
      </div>
    );
  }
}

export default RoutineBlock;
