// React imports
import React from 'react';
// Styled-component imports
import { StyledButton } from './styles';
// Component imports
import RoutineBlock, { BlockCommands } from '../RoutineBlock/RoutineBlock';
import Snackbar from '../../../components/Snackbar/Snackbar';
// React color import
import { CirclePicker } from 'react-color';
// Utility imports
import { isArrayEmpty } from '../../../utils/GeneralUtils';

const START_BLOCK_INDEX = 0;

/**
 * Component that is displayed when in edit view, which allows the user to edit a routine record
 */
class RoutinesEdit extends React.Component {
  /**
   * When the user clicks on the add button of a routine block, insert a new routine command in the routine array
   * after the clicked routine block's command
   */
  addRoutineBlockClickedHandler = (insertIndex) => {
    // Create a copy of the current record's routine array
    const routineBlocks = isArrayEmpty(this.props.currentRecord.routine)
      ? []
      : this.props.currentRecord.routine.slice();
    // Insert a new routine command in the routine array after the clicked routine block's command
    routineBlocks.splice(insertIndex, 0, BlockCommands.forward);
    // Call the parent method to update the current record's state
    this.props.updateRecordHandler('routine', routineBlocks);
  };

  /**
   * When the user clicks on the delete button of a routine block, remove the routine block's command from the
   * routine array
   */
  deleteRoutineBlockClickedHandler = (deleteIndex) => {
    // Create a copy of the current record's routine array
    const routineBlocks = this.props.currentRecord.routine.slice();
    // Remove the routine block's command from the routine array
    routineBlocks.splice(deleteIndex, 1);
    // Call the parent method to update the current record's state
    this.props.updateRecordHandler('routine', routineBlocks);
  };

  /**
   * When the user changes the routine block's command, update the routine block's command in the routine array
   */
  blockCommandChangedHandler = (updateIndex, blockCommand) => {
    // Create a copy of the current record's routine array
    const routineBlocks = this.props.currentRecord.routine.slice();
    // Update the routine block's command in the routine array
    routineBlocks.splice(updateIndex, 1, blockCommand);
    // Call the parent method to update the curent record's state
    this.props.updateRecordHandler('routine', routineBlocks);
  };

  /**
   * Create the routine blocks of the current routine
   */
  createRoutineBlocks = () => {
    let routineBlocks = null;

    if (!isArrayEmpty(this.props.currentRecord.routine)) {
      // If the current record's routine array is not empty, map the items in the array to routine block components
      routineBlocks = this.props.currentRecord.routine.map((routineBlockCommand, index) => {
        return (
          <RoutineBlock
            key={`RoutineBlock-${index}`}
            blockCommand={routineBlockCommand.toUpperCase()}
            addClickedHandler={() => this.addRoutineBlockClickedHandler(index + 1)}
            deleteClickedHandler={() => this.deleteRoutineBlockClickedHandler(index)}
            blockCommandChangedHandler={(blockCommand) => this.blockCommandChangedHandler(index, blockCommand)}
          />
        );
      });
    }

    // Create the start routine block
    const startBlock = (
      <RoutineBlock
        id={'start'}
        blockCommand={BlockCommands.start}
        addClickedHandler={() => this.addRoutineBlockClickedHandler(START_BLOCK_INDEX)}
      />
    );

    return (
      <div className='col-md-6'>
        <div className='col-md-6 mx-auto'>
          {startBlock}
          {routineBlocks}
        </div>
      </div>
    );
  };

  /**
   * Create a header to indicate which routine is being edited
   */
  createHeader = () => {
    return (
      <header id='main-header' className='py-2 bg-primary text-white'>
        <div className='container'>
          <div className='row'>
            <div className='col-md-6'>
              <h1>{`Edit - ${this.props.currentRecord.name}`}</h1>
            </div>
          </div>
        </div>
      </header>
    );
  };

  /**
   * Create the edit view action buttons
   */
  createActions = () => {
    // Create the back button which, when clicked on, navigates the user back to the grid view
    const backButton = (
      <button id='back-icon' className='btn btn-light btn-block' onClick={this.props.backClickedHandler}>
        <i className='fas fa-undo' /> To Grid View
      </button>
    );

    // Create the save button which, when clicked on, saves the current routine record in the database
    const saveButton = (
      <StyledButton
        id='save-icon'
        className='btn btn-primary btn-block'
        onClick={this.props.saveClickedHandler}
        disabled={this.props.routineNameEmpty || this.props.routineNameTooLong || this.props.routineNameDuplicate}
        disableCursor={this.props.routineNameEmpty || this.props.routineNameTooLong || this.props.routineNameDuplicate}
      >
        <i className='fas fa-save' /> Save Changes
      </StyledButton>
    );

    // Create the play button which, when clicked on, navigates the user to the play view
    const playButton = (
      <button id='play-icon' className='btn btn-success btn-block' onClick={this.props.playClickedHandler}>
        <i className='fas fa-play' /> Play Routine
      </button>
    );

    return (
      <section id='actions' className='py-4 mb-4'>
        <div className='container'>
          <div className='row'>
            <div className='col-md-3'>{backButton}</div>
            <div className='col-md-3'>{saveButton}</div>
            <div className='col-md-3'>{playButton}</div>
          </div>
        </div>
      </section>
    );
  };

  /**
   * Create the details form component where the user can specify the name of the routine as well as the color of
   * the spaceship
   */
  createDetails = () => {
    let routineNameError;
    let routineNameClass;
    if (this.props.routineNameEmpty) {
      // If the routine name is empty, display an error message underneath the routine name field
      routineNameError = <div className='invalid-feedback'>Routine name is required</div>;
      routineNameClass = 'form-control is-invalid';
    } else if (this.props.routineNameTooLong) {
      // If the routine name is more than 30 characters, display an error message underneath the routine name field
      routineNameError = <div className='invalid-feedback'>Routine name is too long</div>;
      routineNameClass = 'form-control is-invalid';
    } else if (this.props.routineNameDuplicate) {
      // If the routine name already exists, display an error message underneath the routine name field
      routineNameError = <div className='invalid-feedback'>Routine name has already been taken</div>;
      routineNameClass = 'form-control is-invalid';
    } else {
      routineNameError = null;
      routineNameClass = 'form-control';
    }

    // Create the routine name field
    const routineNameField = (
      <div className='form-group'>
        <label htmlFor='name'>Routine name</label>
        <input
          id='name'
          type='text'
          className={routineNameClass}
          value={this.props.currentRecord.name}
          onChange={(event) => this.props.updateRecordHandler('name', event.target.value)}
        />
        {routineNameError}
      </div>
    );

    // Create the spaceship color field
    const spaceshipColorField = (
      <div className='form-group'>
        <label htmlFor='name'>Spaceship color</label>
        <CirclePicker
          color={this.props.currentRecord.color}
          onChangeComplete={(color) => this.props.updateRecordHandler('color', color.hex)}
        />
      </div>
    );

    return (
      <div className='col-md-4'>
        <div className='card p-2'>
          <div className='card-body'>
            <h4>Details</h4>
            <hr />
            <div className='row'>
              <div className='col-md-12'>
                {routineNameField}
                {spaceshipColorField}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  };

  /**
   * Render the RoutinesEdit component
   */
  render() {
    return (
      <div>
        {this.createHeader()}
        {this.createActions()}
        <section id='edit'>
          <div className='container'>
            <div className='row'>
              {this.createDetails()}
              {this.createRoutineBlocks()}
            </div>
          </div>
        </section>
        <Snackbar show={this.props.showSnackbar} message={this.props.snackbarMessage} />
      </div>
    );
  }
}

export default RoutinesEdit;
