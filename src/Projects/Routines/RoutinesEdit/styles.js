// Styled-components import
import styled from 'styled-components';

export const StyledButton = styled.button`
  cursor: ${(props) => (props.disableCursor ? 'not-allowed' : 'pointer')} !important;
`;
