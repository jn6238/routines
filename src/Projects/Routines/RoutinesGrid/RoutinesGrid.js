// React imports
import React from 'react';
// Component imports
import Snackbar from '../../../components/Snackbar/Snackbar';
// Styled-component imports
import { StyledCell } from './styles';
// Moment imports
import moment from 'moment';
// Utility imports
import { isNull } from '../../../utils/GeneralUtils';

const DATE_TIME_FORMAT = 'DD-MM-YYYY HH:mm';

/**
 * Component that is displayed when in grid view, which displays all the created routine records
 */
class RoutinesGrid extends React.Component {
  /**
   * On component mount, fetch the grid data from the database
   */
  componentDidMount() {
    this.props.fetchRoutines();
  }

  /**
   * Convert the routine data to grid records
   */
  getRoutines = () => {
    let routines = [];
    if (!isNull(this.props.routines)) {
      // If the routine data is not empty, loop through the object
      for (const [key, routine] of Object.entries(this.props.routines)) {
        // Convert the last changed date to a user friendly date format
        const lastChangedDate = moment(routine.lastChanged).local().format(DATE_TIME_FORMAT);

        // Add a play button to the record which, when clicked on, navigates the user to the play view
        // and plays the record's routine
        const playButton = (
          <button className='btn' onClick={() => this.props.playClickedHandler(routine, key)}>
            <i className='fas fa-play' /> Play
          </button>
        );

        // Add an edit button to the record which, when clicked on, navigates the user to the edit view
        // allowing the user to edit the record
        const editButton = (
          <button className='btn' onClick={() => this.props.editClickedHandler(routine, key)}>
            <i className='fas fa-edit' /> Edit
          </button>
        );

        // Add a delete button to the record which, when clicked on, deletes the record
        const deleteButton = (
          <button className='btn' onClick={() => this.props.deleteClickedHandler(key)}>
            <i className='fas fa-trash' /> Delete
          </button>
        );

        // Add a record to the array of records to be displayed in the grid
        routines.push(
          <tr key={key}>
            <StyledCell>{routine.name}</StyledCell>
            <StyledCell>{lastChangedDate}</StyledCell>
            <StyledCell>{playButton}</StyledCell>
            <StyledCell>{editButton}</StyledCell>
            <StyledCell>{deleteButton}</StyledCell>
          </tr>
        );
      }
    }
    return routines;
  };

  /**
   * Create the add routine action which will navigate the user to the edit view and allow the user
   * to create a new routine record
   */
  createAddRoutineAction = () => {
    return (
      <div className='row py-5'>
        <div className='col-md-4 mx-auto'>
          <button className='btn btn-primary btn-block' onClick={this.props.addRoutineClickedHandler}>
            <i className='fas fa-plus' /> Add Routine
          </button>
        </div>
      </div>
    );
  };

  /**
   * Render the RoutinesGrid component
   */
  render() {
    return (
      <div>
        <div className='container'>
          {this.createAddRoutineAction()}
          <div className='row'>
            <div className='col-md-12'>
              <div className='card'>
                <div className='card-header'>
                  <h4>Spaceship Routines</h4>
                </div>
                <table className='table table-striped'>
                  <thead className='thead-dark'>
                    <tr>
                      <th>Name</th>
                      <th>Last Changed</th>
                      <th />
                      <th />
                      <th />
                    </tr>
                  </thead>
                  <tbody>{this.getRoutines()}</tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
        <Snackbar show={this.props.showSnackbar} message={this.props.snackbarMessage} />
      </div>
    );
  }
}

export default RoutinesGrid;
