// Styled-components import
import styled from 'styled-components';

export const StyledCell = styled.td`
  vertical-align: middle !important;
`;
