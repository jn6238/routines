// React imports
import React from 'react';
// Component imports
import LayoutContainer from '../../components/Layout/Layout';
import RoutinesEdit from './RoutinesEdit/RoutinesEdit';
import RoutinesGrid from './RoutinesGrid/RoutinesGrid';
import RoutinesPlay from './RoutinesPlay/RoutinesPlay';
import ErrorModal from './ErrorModal/ErrorModal';
// Utility imports
import { deleteRoutine, fetchRoutines, saveRoutine } from '../../utils/AjaxUtils';
import { isArrayEmpty, isStringEmpty } from '../../utils/GeneralUtils';
// Crypto random string import
import cryptoRandomString from 'crypto-random-string';

// Contstants used to represent the screen view which should be shown
const screenViews = {
  editView: 'editView',
  gridView: 'gridView',
  playView: 'playView',
};

/**
 * The parent of the routine grid, edit and play classes which manages the state of the routines
 */
class Routines extends React.Component {
  state = {
    // Flag to indicate whether a Ajax call to the database failed
    ajaxError: false,
    // The routines data object
    data: null,
    // The record being edited
    editRecord: null,
    // The key of the record being edited
    editRecordKey: null,
    // Flag to indicate that the routine name being edited has already been taken
    routineNameDuplicate: false,
    // Flag to indicate whether the routine name being edited is empty
    routineNameEmpty: false,
    // Flag to indicate whether the length of the routine name being edited is too long
    routineNameTooLong: false,
    // Used to decide whether to display the grid, edit or play view
    screenView: screenViews.gridView,
    // Flag to indicate whether to display the snackbar
    showSnackbar: false,
    // Message to display on the snackbar
    snackbarMessage: null,
  };

  // Default object when creating a new routine
  createDefaultRoutineObject = {
    name: 'New Routine',
    color: '#FFFFFF',
    lastChanged: null,
    routine: [],
  };

  /**
   * Check whether the edited routine name has already been taken by another routine
   */
  checkRoutineNameDupliacte = (newRoutineName) => {
    for (const key in this.state.data) {
      if (
        this.state.data.hasOwnProperty(key) &&
        key !== this.state.editRecordKey &&
        this.state.data[key].name === newRoutineName
      ) {
        return true;
      }
    }
    return false;
  };

  /**
   * Update the state when a record is edited on the edit view
   */
  updateRecordHandler = (field, value) => {
    let { routineNameDuplicate, routineNameEmpty, routineNameTooLong } = this.state;

    if (field === 'name') {
      // If the routine name field is being updated, perform validation to check that the name
      // is not empty, does not exceed 30 characters and has not already been taken by another routine
      routineNameEmpty = isStringEmpty(value);
      routineNameTooLong = !isStringEmpty(value) && value.length > 30;
      routineNameDuplicate = this.checkRoutineNameDupliacte(value);
    }

    this.setState((prevState) => {
      return {
        ...prevState,
        editRecord: {
          ...prevState.editRecord,
          [field]: value,
        },
        routineNameDuplicate,
        routineNameEmpty,
        routineNameTooLong,
      };
    });
  };

  /**
   * Set the grid data
   */
  setGridData = (data) => {
    this.setState((prevState) => {
      return {
        ...prevState,
        ajaxError: false,
        data,
      };
    });
  };

  /**
   * Upon successful deletion of a routine, show a snackbar message and set the new grid data
   */
  deleteRecordSuccessHandler = (data) => {
    this.setState(
      (prevState) => {
        return {
          ...prevState,
          ajaxError: false,
          showSnackbar: true,
          snackbarMessage: 'Delete Successful',
          data,
        };
      },
      // Hide the snackbar after 2 seconds
      () => {
        setTimeout(() => {
          this.setState((prevState) => {
            return {
              ...prevState,
              showSnackbar: false,
            };
          });
        }, 2000);
      }
    );
  };

  /**
   * Upon succesful saving of a routine, show a snackbar message
   */
  saveRecordSuccessHandler = () => {
    this.setState(
      (prevState) => {
        return {
          ...prevState,
          ajaxError: false,
          showSnackbar: true,
          snackbarMessage: 'Save Successful',
        };
      },
      // Hide the snackbar after 2 seconds
      () => {
        setTimeout(() => {
          this.setState((prevState) => {
            return {
              ...prevState,
              showSnackbar: false,
            };
          });
        }, 2000);
      }
    );
  };

  /**
   * When the user adds a new routine, create a default routine record and navigate to the edit view
   */
  gridAddRoutineClickedHandler = () => {
    this.setState((prevState) => {
      return {
        ...prevState,
        editRecord: { ...this.createDefaultRoutineObject },
        editRecordKey: cryptoRandomString({ length: 20, type: 'numeric' }),
        screenView: screenViews.editView,
      };
    });
  };

  /**
   * When the user clicks on the edit button in the grid, navigate to the edit view and set the edit
   * record to the selected grid record
   */
  gridEditRecordClickedHandler = (routine, key) => {
    this.setState((prevState) => {
      return {
        ...prevState,
        screenView: screenViews.editView,
        editRecord: routine,
        editRecordKey: key,
      };
    });
  };

  /**
   * When the user clicks on the play button in the grid, navigate to the play view and set the edit
   * record to the selected grid record
   */
  gridPlayRecordClickedHandler = (routine, key) => {
    this.setState((prevState) => {
      return {
        ...prevState,
        screenView: screenViews.playView,
        editRecord: routine,
        editRecordKey: key,
      };
    });
  };

  /**
   * When the user clicks on the back button on the edit view, navigate back to the grid view and
   * reset the edit record and validation flags
   */
  editBackClickedHandler = () => {
    this.setState((prevState) => {
      return {
        ...prevState,
        screenView: screenViews.gridView,
        editRecord: null,
        editRecordKey: null,
        routineNameDuplicate: false,
        routineNameEmpty: false,
        routineNameTooLong: false,
      };
    });
  };

  /**
   * When the user clicks on the play button on the edit view, navigate to the play view
   */
  editPlayClickedHandler = () => {
    this.setState((prevState) => {
      return {
        ...prevState,
        screenView: screenViews.playView,
      };
    });
  };

  /**
   * When the user clicks on the save button on the edit view, update the current record's last changed date,
   * before performing an Ajax call to update the record in the database
   */
  editSaveClickedHandler = () => {
    // Copy the routine array of the edit record
    const copyRoutine = isArrayEmpty(this.state.editRecord.routine) ? [] : [...this.state.editRecord.routine];

    // Copy the edit record and update the last changed date
    const copyRecord = {
      ...this.state.editRecord,
      routine: copyRoutine,
      lastChanged: new Date(),
    };

    // Perform an Ajax call to update the record in the database
    saveRoutine(this.state.editRecordKey, copyRecord, this.saveRecordSuccessHandler, this.ajaxErrorHandler);
  };

  /**
   * When the user clicks on the back button on the play view, navigate back to the edit view
   */
  playBackClickedHandler = () => {
    this.setState((prevState) => {
      return {
        ...prevState,
        screenView: screenViews.editView,
      };
    });
  };

  /**
   * When the Ajax call to the database fails, set the ajaxError flag to true
   */
  ajaxErrorHandler = () => {
    this.setState((prevState) => {
      return {
        ...prevState,
        ajaxError: true,
      };
    });
  };

  /**
   * Close the error modal
   */
  closeErrorModalHandler = () => {
    this.setState((prevState) => {
      return {
        ...prevState,
        ajaxError: false,
      };
    });
  };

  /**
   * Create the routine grid view component
   */
  createRoutineGridComponent = () => {
    return (
      <RoutinesGrid
        addRoutineClickedHandler={this.gridAddRoutineClickedHandler}
        editClickedHandler={this.gridEditRecordClickedHandler}
        playClickedHandler={this.gridPlayRecordClickedHandler}
        deleteClickedHandler={(routineKey) =>
          deleteRoutine(
            routineKey,
            () => fetchRoutines(this.deleteRecordSuccessHandler, this.ajaxErrorHandler),
            this.ajaxErrorHandler
          )
        }
        fetchRoutines={() => fetchRoutines(this.setGridData, this.ajaxErrorHandler)}
        routines={this.state.data}
        showSnackbar={this.state.showSnackbar}
        snackbarMessage={this.state.snackbarMessage}
      />
    );
  };

  /**
   * Create the routine edit view component
   */
  createRoutineEditComponent = () => {
    return (
      <RoutinesEdit
        currentRecord={this.state.editRecord}
        backClickedHandler={this.editBackClickedHandler}
        playClickedHandler={this.editPlayClickedHandler}
        saveClickedHandler={this.editSaveClickedHandler}
        updateRecordHandler={this.updateRecordHandler}
        showSnackbar={this.state.showSnackbar}
        snackbarMessage={this.state.snackbarMessage}
        routineNameDuplicate={this.state.routineNameDuplicate}
        routineNameEmpty={this.state.routineNameEmpty}
        routineNameTooLong={this.state.routineNameTooLong}
      />
    );
  };

  /**
   * Create the routine play view component
   */
  createRoutinePlayComponent = () => {
    return (
      <RoutinesPlay
        currentRecord={this.state.editRecord}
        backClickedHandler={this.playBackClickedHandler}
        playClickedHandler={this.playPlayClickedHandler}
        stopClickedHandler={this.playStopClickedHandler}
      />
    );
  };

  /**
   * Render the Routines component
   */
  render() {
    let content;
    switch (this.state.screenView) {
      case screenViews.editView:
        content = this.createRoutineEditComponent();
        break;
      case screenViews.playView:
        content = this.createRoutinePlayComponent();
        break;
      case screenViews.gridView:
      default:
        content = this.createRoutineGridComponent();
        break;
    }

    // If there has been an error in the Ajax call to the database, display the error modal
    const errorModal = this.state.ajaxError ? <ErrorModal closeModalHandler={this.closeErrorModalHandler} /> : null;

    return (
      <LayoutContainer>
        {content}
        {errorModal}
      </LayoutContainer>
    );
  }
}

export default Routines;
