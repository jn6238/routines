// React imports
import React from 'react';
import ReactDOM from 'react-dom';
// Router imports
import { BrowserRouter } from 'react-router-dom';
// Style imports
import './index.css';
// Component import
import App from './App';
// Service worker import
import * as serviceWorker from './serviceWorker';
// Bootstrap import
import 'bootstrap/dist/css/bootstrap.css';

const app = (
  <BrowserRouter>
    <App />
  </BrowserRouter>
);

ReactDOM.render(app, document.getElementById('root'));

serviceWorker.unregister();
