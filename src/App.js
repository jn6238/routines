// React import
import React from 'react';
// Router imports
import { Redirect, Route, Switch, withRouter } from 'react-router-dom';
// Component import
import Projects, { projects } from './Projects/Projects';

/**
 * Root Application component
 */
class App extends React.Component {
  render() {
    const routes = projects.map((project) => (
      <Route key={project.path} path={project.path} component={project.component} />
    ));

    // Set /routines as the base route while redirecting all routes to the base route.
    const contents = (
      <Switch>
        <Route path='/projects' exact component={Projects} />
        {routes}
        <Redirect from='/*' to='/projects' />
      </Switch>
    );
    return (
      <div className='App'>
        <link
          rel='stylesheet'
          href='https://use.fontawesome.com/releases/v5.0.13/css/all.css'
          integrity='sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp'
          crossOrigin='anonymous'
        />
        <link
          rel='stylesheet'
          href='https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css'
          integrity='sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh'
          crossOrigin='anonymous'
        />
        {contents}
      </div>
    );
  }
}

export default withRouter(App);
