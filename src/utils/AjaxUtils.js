// Constants
const HTTP_RESPONSE_READY = 4;
const HTTP_STATUS_OK = 200;

/**
 * Perform a call to fetch the routines from the database
 */
export const fetchRoutines = (successCallback, failedCallback) => {
  const httpRequest = new XMLHttpRequest();
  httpRequest.onreadystatechange = function() {
    if (httpRequest.readyState === HTTP_RESPONSE_READY) {
      if (httpRequest.status === HTTP_STATUS_OK) {
        // Execute the success callback
        successCallback(JSON.parse(httpRequest.response));
      } else {
        // Execute the failed callback
        failedCallback();
      }
    }
  };
  httpRequest.open('GET', 'https://routines-6d342.firebaseio.com/data.json');
  httpRequest.send();
};

/**
 * Perform a call to delete the routine with the specified key from the database
 */
export const deleteRoutine = (routineKey, successCallback, failedCallback) => {
  const httpRequest = new XMLHttpRequest();
  httpRequest.onreadystatechange = function() {
    if (httpRequest.readyState === HTTP_RESPONSE_READY) {
      if (httpRequest.status === HTTP_STATUS_OK) {
        // Execute the success callback
        successCallback(JSON.parse(httpRequest.response));
      } else {
        // Execute the failed callback
        failedCallback();
      }
    }
  };
  httpRequest.open('DELETE', `https://routines-6d342.firebaseio.com/data/${routineKey}.json`);
  httpRequest.send();
};

/**
 * Perform a call to update the routine with the specified key in the database
 */
export const saveRoutine = (routineKey, routine, successCallback, failedCallback) => {
  const httpRequest = new XMLHttpRequest();
  httpRequest.onreadystatechange = function() {
    if (httpRequest.readyState === HTTP_RESPONSE_READY) {
      if (httpRequest.status === HTTP_STATUS_OK) {
        // Execute the success callback
        successCallback();
      } else {
        // Execute the failed callback
        failedCallback();
      }
    }
  };
  httpRequest.open('PUT', `https://routines-6d342.firebaseio.com/data/${routineKey}.json`);
  httpRequest.send(JSON.stringify(routine));
};
