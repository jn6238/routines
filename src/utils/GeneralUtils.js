/**
 * Check if a value is null or undefined
 */
export const isNull = (value) => value === null || typeof value === 'undefined';

/**
 * Check if an array is empty
 */
export const isArrayEmpty = (arr) => arr === undefined || arr === null || arr.length <= 0;

/**
 * Check if a string is empty
 */
export const isStringEmpty = (value) => isNull(value) || value.trim() === '';
